# Deploy Application from Jenkins Pipeline to EC2 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

* Prepared AWS EC2 instance for deployment (installDocker)

* Created ssh key credentials for EC2 server on Jenkins

* Extended the previous CI pipeline with deploy step to ssh into the remote EC2 instance and deployed  built image from Jenkins server

* Configured security group on EC2 Instance to allow access to our web application

## Technologies used 

* AWS 

* Jenkins

* Docker 

* Linux 

* Java 

* Maven 

* Docker Hub

## Steps 

Step 1: Install SSH Agent plugin on Jenkins

[Installing SSH Agent](/images/01_downloading_ssh_agent.png)

Step 2: Add sshkey in multibranch credentials 

[sshkey added to multibranch](/images/02_adding_sshkey_in_multibranch_scope_for_ec2_server_in_jenkins.png)

Step 3: In Jenkinsfile add deploy stage and give it commands to ssh into EC2 instance and deploy application through running the container from dockerhub 

[Jenkinsfile deploy stage](/images/03_ssh_into_ec2_and_deploying_application_through_jenkinsfile.png)  

Step 4: Open the port in which Jenkins Listens on and add Jenkins server ip address to have access to ssh port 22

[Network configuration](/images/04_adding_jenkins_ip_address_to_have_access_to_por22.png)

Step 5: Check pipeline to see if it was successful 

[Successful Pipeline](/images/05_successful_pipeline.png)
[logs](/images/06_logs.png)
[running container](/images/08_container_running_on_server.png)
[application on browser](/images/07_on_browser.png)

## Installation

Run $ sudo yum install docker 

## Usage 

Run $ java -jar java-maven-app*.jar 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/cd-with-jenkins-and-aws.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using mvn test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/cd-with-jenkins-and-aws

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.